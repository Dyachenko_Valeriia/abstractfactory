import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class TestShop {
    @Test
    public void testShop() {
        ChocolateSweets chocolateSweet = new ChocolateSweets("abc", 20);
        JellySweets jellySweets = new JellySweets("cde", 10);
        Lollipop lollipop = new Lollipop("def", 5);
        RussianFactory russianFactory = new RussianFactory(chocolateSweet);
        AmericanFactory americanFactory = new AmericanFactory(jellySweets);
        RussianFactory redOctober = new RussianFactory(lollipop);
        Set<String> expected = new HashSet<>();
        Collections.addAll(expected, "Party of Russian chocolates", "Party of American jelly sweets", "Party of Russian lollipop");
        Set<String> actual = new HashSet<>();
        Collections.addAll(actual, russianFactory.sort(), americanFactory.sort(), redOctober.sort());
        assertEquals(expected, actual);
    }
}
