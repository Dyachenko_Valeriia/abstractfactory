public class RussianFactory implements AbstractFactory{
    private Sweets sweets;
    public RussianFactory(Sweets sweets){
        this.sweets = sweets;
    }
    @Override
    public String sort() {
        String result = null;
        if(sweets.getCompos().equals(Composition.CHOCOLATE)){
            result = "Party of Russian chocolates";
        }
        if(sweets.getCompos().equals(Composition.JELLY)){
            result = "Party of Russian jelly sweets";
        }
        if(sweets.getCompos().equals(Composition.MOLASSES)){
            result = "Party of Russian lollipop";
        }
        return result;
    }
}
