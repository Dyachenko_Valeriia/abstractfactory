import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Shop {
    public static Set<String> supplySweets(RussianFactory russianFactory, AmericanFactory americanFactory){
        Set<String> result = new HashSet<>();
        Collections.addAll(result, russianFactory.sort(), americanFactory.sort());
        return result;
    }
}
