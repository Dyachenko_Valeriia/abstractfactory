public class AmericanFactory implements AbstractFactory{
    private Sweets sweets;
    public AmericanFactory(Sweets sweets){
        this.sweets = sweets;
    }
    @Override
    public String sort() {
        String result = null;
        if(sweets.getCompos().equals(Composition.CHOCOLATE)){
            result = "Party of American chocolates";
        }
        if(sweets.getCompos().equals(Composition.JELLY)){
            result = "Party of American jelly sweets";
        }
        if(sweets.getCompos().equals(Composition.MOLASSES)){
            result = "Party of American lollipop";
        }
        return result;
    }
}
